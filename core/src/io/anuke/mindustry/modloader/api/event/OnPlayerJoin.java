package io.anuke.mindustry.modloader.api.event;

import io.anuke.mindustry.entities.Player;
import io.anuke.mindustry.modloader.api.Event;

public class OnPlayerJoin extends Event {

    public Player player;

    public OnPlayerJoin(Player player) {
        super("onPlayerJoin");
        this.player = player;
    }
}
