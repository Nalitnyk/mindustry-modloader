package io.anuke.mindustry.modloader.objects;

import com.badlogic.gdx.utils.Array;
import io.anuke.mindustry.modloader.objects.Mod;
import io.anuke.mindustry.modloader.utils.Logger;

public class ModContainer {
    private Array<Mod> mods = new Array<>();

    public ModContainer() {}

    public boolean addMod(Mod mod) {
        if(mods.contains(mod, false)) return false;
        mod.id = mods.size;
        mods.add(mod);
        return true;
    }

    public void delMod(Mod mod){
        if(!mod.file.delete()) Logger.err("Failed to remove file of {0} mod", mod.modInfo.name);
        mods.removeIndex(mod.id);
    }

    public Array<Mod> getMods() {
        return mods;
    }

    public String[] getModUUIDs(){
        String[] s = new String[mods.size];

        int i = 0;
        for (Mod m : mods) {
            s[i++] = m.uuid;
        }

        return s;
    }

    public Mod getModByName(String name) {
        for (Mod m : mods) {
            if(m.modInfo.name.equals(name)) return m;
        }
        return null;
    }

    public Mod getModByID(int id) {
        return mods.get(id);
    }

    public Mod getModByUUID(String uuid){
        for (Mod m : mods) {
            if(m.uuid.equals(uuid)) return m;
        }
        return null;
    }
}
