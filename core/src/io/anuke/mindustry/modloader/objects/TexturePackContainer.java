package io.anuke.mindustry.modloader.objects;

import com.badlogic.gdx.utils.Array;
import io.anuke.mindustry.modloader.utils.Logger;

public class TexturePackContainer {
    private Array<TexturePack> texturePacks = new Array<>();

    public TexturePackContainer() {}

    public boolean addTexturePack(TexturePack texturePack) {
        if(texturePacks.contains(texturePack, false)) return false;
        texturePacks.add(texturePack);
        return true;
    }

    public boolean removeTexturePack(TexturePack texturePack) {
        if(texturePacks.contains(texturePack, false))
            texturePacks.removeValue(texturePack, false);
        else
            return false;
        return true;
    }

    public Array<TexturePack> getTexturePacks() {
        return texturePacks;
    }

    public TexturePack getTexturePackByName(String name) {
        for (TexturePack m : texturePacks) {
            if(m.info.name.equals(name)) return m;
        }
        return null;
    }

    public void movePriority(Integer position, boolean up){
        try {
            texturePacks.swap(position, up ? position - 1 : position + 1);
        } catch(Exception ignored){}
    }

    public void flipContainer(){
        texturePacks.reverse();
    }
}
