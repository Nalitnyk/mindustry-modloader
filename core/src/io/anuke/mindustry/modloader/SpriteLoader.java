package io.anuke.mindustry.modloader;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.TextureData;
import com.badlogic.gdx.graphics.g2d.Gdx2DPixmap;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import io.anuke.mindustry.Vars;
import io.anuke.mindustry.modloader.objects.Mod;
import io.anuke.mindustry.modloader.utils.Logger;
import io.anuke.ucore.core.Core;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Objects;

public class SpriteLoader {

    private static Mod mod;

    public static void loadSprites(Mod modIn, ArrayList<Path> paths) {
        try {
            Path texPath = Paths.get(Vars.modDirectory + "/tmp/textures");
            if (!Files.exists(texPath)) Files.createDirectories(texPath);

            //this can be removed later
            if(Objects.requireNonNull(texPath.toFile().listFiles()).length != 0) {
                for (File file : Objects.requireNonNull(texPath.toFile().listFiles())) {
                    if(!file.delete()) Logger.warn("Failed to delete file {0}", file.getName());
                }
            }

            for (Path path : paths) {
                if (!Files.isDirectory(path) && path.toString().contains("sprites")) {
                    InputStream is = Files.newInputStream(path);
                    Files.copy(is, Paths.get(texPath.toString() +"/"+ path.getFileName().toString()));
                }
            }

            mod = modIn;

            Files.walk(texPath)
                    .parallel()
                    .filter(Files::isRegularFile)
                    .forEach(SpriteLoader::addTexture);

            Logger.print("Loaded Atlas for mod: {0}", mod.modInfo.name);

            if(!texPath.toFile().delete()) Logger.warn("Failed to remove tmp/textures");

            mod = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void addTexture(Path path) {
        FileHandle f = new FileHandle(path.toFile());
        Pixmap p = new Pixmap(f);
        Texture t = new Texture(p);
        TextureData td = t.getTextureData();

        if(f.nameWithoutExtension().equals("icon")) mod.icon = new TextureRegion(t);
        else Core.atlas.addRegion(mod.modInfo.id + ":" + f.nameWithoutExtension(), t, 0,0, td.getWidth(), td.getHeight());
    }
}
