package io.anuke.mindustry.ui.dialogs;

import com.badlogic.gdx.utils.Base64Coder;
import com.badlogic.gdx.utils.Json;
import com.esotericsoftware.yamlbeans.YamlReader;
import io.anuke.mindustry.Vars;
import io.anuke.mindustry.core.*;
import io.anuke.mindustry.modloader.objects.Mod;
import io.anuke.mindustry.modloader.objects.ModInfo;
import io.anuke.ucore.core.Core;
import io.anuke.ucore.core.Settings;
import io.anuke.ucore.core.Timers;
import io.anuke.ucore.scene.ui.Label;
import io.anuke.ucore.scene.ui.ScrollPane;
import io.anuke.ucore.scene.ui.layout.Table;
import io.anuke.ucore.util.Bundles;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.stream.Collectors;

import static io.anuke.mindustry.Vars.*;

public class ModsDialog extends FloatingDialog {

    ScrollPane pane;
    private Table slots;

    public ModsDialog() {
        this("$text.mods");
    }

    public ModsDialog(String title) {
        super(title);
        setup();

        shown(() -> {
            setup();
            Timers.runTask(2f, () -> Core.scene.setScrollFocus(pane));
        });

        addCloseButton();
    }

    protected void setup() {
        content().clear();

        slots = new Table();
        pane = new ScrollPane(slots, "clear-black");
        pane.setFadeScrollBars(false);
        pane.setScrollingDisabled(true, false);

        slots.marginRight(24);

        Timers.runTask(2f, () -> Core.scene.setScrollFocus(pane));

        for (Mod slot : Vars.modloader.getMods()) {
            Table txtbutton = new Table("pane");
            txtbutton.add("[accent]" + slot.modInfo.name).left().height(20);
            txtbutton.row();

            txtbutton.addImage(slot.icon).size(64, 64).left().expand();

            String color = "[lightgray]";

            txtbutton.add(String.format(Bundles.format("text.editor.author") + " %s",
                    color + (slot.modInfo.author == null ? "Unknown" : slot.modInfo.author))).left().top().pad(0);

            Table t = new Table();
            {
                t.right();

                t.addImageButton(slot.enabled ? "icon-enabled" : "icon-disabled", "emptytoggle", 14 * 3, () -> {
                    slot.enabled = !slot.enabled;
                    if (slot.enabled)
                        Vars.modloader.enabledMods.add(slot.uuid);
                    else
                        Vars.modloader.enabledMods.removeValue(slot.uuid, false);
                    setup();
                }).checked(slot.enabled).right();

                t.addImageButton("icon-gear", "empty", 14 * 3, () -> {
                    ui.config.setup(slot.uuid);
                    ui.config.show();
                });

                t.addImageButton("icon-trash", "empty", 14 * 3, () ->
                        ui.showConfirm("$text.confirm", "$text.mods.delete.confirm", () -> {
                            Vars.modloader.delmod(slot);
                            setup();
                        })
                ).size(14 * 3).right();
            }

            txtbutton.add(t).right();

            txtbutton.row();
            txtbutton.add(String.format(Bundles.format("text.mods.desc", (slot.modInfo.desc == null ? "Unknown" : slot.modInfo.desc)) + " %s",
                    color + (slot.modInfo.desc == null ? "Unknown" : slot.modInfo.desc))).left();
            txtbutton.row();

            //.padRight(-10).growX().right();
            slots.add(txtbutton).uniformX().fillX().pad(4).padRight(-4).margin(10f).marginLeft(20f).marginRight(20f).colspan(2);

            slots.row();
        }

        content().add(pane);

        addSetup();
    }

    private void addSetup() {
        if (Vars.modloader.getMods().size == 0) {
            Table t = new Table("pane");
            Label l = new Label("$text.mods.none");
            t.add(l);
            slots.add(t.center());
        }

        slots.row();

        if (gwt || ios) return;

        slots.addImageTextButton("$text.mods.apply", "icon-check", "clear", 14 * 3, () ->
                ui.showConfirm("$text.confirm", "$text.mods.apply.confirm", () -> {
                    Settings.putJson("enabledMods", Vars.modloader.enabledMods);
                    Settings.save();
                    setup();
                })).fillX().margin(10f).minWidth(300f).height(70f).pad(4f).padRight(-4);

        slots.addImageTextButton("$text.mods.import", "icon-add", "clear", 14 * 3, () ->
                Platform.instance.showFileChooser(Bundles.get("text.mods.import"), "Mod Jar", file -> {
                    try {
                        if (!file.toString().equals(Vars.modDirectory + "/" + file.name())) {
                            file.copyTo(Vars.modDirectory);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {
                        Mod mod = new Mod();

                        URI resURI = new URI("jar:" + file.file().toURI().toString());
                        FileSystem fileSystem = FileSystems.newFileSystem(resURI, Collections.emptyMap());
                        Path path = fileSystem.getPath("modmeta");
                        if(Files.exists(path)) {
                            InputStream input = Files.newInputStream(path);
                            YamlReader yr = new YamlReader(new InputStreamReader(input, StandardCharsets.UTF_8));

                            mod.modInfo = yr.read(ModInfo.class);
                            mod.uuid = Base64Coder.encodeString(mod.modInfo.author + mod.modInfo.name);

                            modloader.addModToContainer(mod);
                            ui.showInfo("$text.mods.import.success");
                            setup();
                        } else {
                            ui.showError("$text.mods.import.notMod");
                            file.delete();
                        }
                    } catch (Exception e) { e.printStackTrace(); }
                }, true, "jar")).fillX().margin(10f).minWidth(300f).height(70f).pad(4f).padRight(-4);
    }
}