package com.examplemod.block;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.examplemod.content.ModMechs;
import io.anuke.mindustry.Vars;
import io.anuke.mindustry.content.Items;
import io.anuke.mindustry.type.Category;
import io.anuke.mindustry.type.ItemStack;
import io.anuke.mindustry.type.Recipe;
import io.anuke.mindustry.world.Block;
import io.anuke.mindustry.world.Tile;
import io.anuke.mindustry.world.blocks.units.MechFactory;
import io.anuke.ucore.graphics.Draw;

public class ModBlocks {

    public static void onInitialization() {
        Block exampleMechFactory = new MechFactory("examplemod:example-mech-factory") {
            {
                mech = ModMechs.exampleMech;
                size = 2;
            }
        };

        Block exampleSmallWall = new ExampleWall("examplemod:smallExampleWall");

        new Recipe(Category.units, exampleMechFactory, new ItemStack(Items.copper, 1));
        new Recipe(Category.defense, exampleSmallWall, new ItemStack(Items.copper, 1));
    }
}
