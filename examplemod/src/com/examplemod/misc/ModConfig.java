package com.examplemod.misc;

import com.examplemod.examplemod;
import io.anuke.mindustry.modloader.api.Config;

import java.util.HashMap;
import java.util.Map;

public class ModConfig {

    public static void createConfig() {
        Map map = new HashMap<>();
        map.put("defog", false);
        Config.writeConfig("example",map);
        examplemod.config = map;
    }

}
