package com.examplemod;

import com.examplemod.block.ModBlocks;
import com.examplemod.content.ModMechs;
import com.examplemod.misc.ModConfig;
import com.examplemod.misc.ModMisc;
import com.examplemod.weapon.ModWeapons;
import io.anuke.mindustry.modloader.Annotations;
import io.anuke.mindustry.modloader.api.Config;
import java.util.Map;

@Annotations.Mod
public class examplemod {
    public static Map config;

    @Annotations.MMLEvent(event = Annotations.Event.preinit)
    public static void onPreInitialization() {
        if(Config.configExist("example")) config = Config.getConfig("example");
        else ModConfig.createConfig();
    }

    @Annotations.MMLEvent(event = Annotations.Event.init)
    public static void onInitialization() {
        ModWeapons.onInitialization();
        ModMechs.onInitialization();
        ModBlocks.onInitialization();
    }

    @Annotations.MMLEvent(event = Annotations.Event.postinit)
    public static void onPostInitialization() {
        ModMisc.onPostInitialization();
    }
}
